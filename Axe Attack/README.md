This mod allows players to hunt animals using Axes, in addition to Bows. Dealing damage rewards experience in the Hunting skill.

This isn't a .override, and Eco's built-in mechanisms for damage dealing, tool usage, and skill mechanisms are thoroughly utilized (to the best of my ability) so modifiers and other utilities (from other mods, or built-in) should all apply correctly. 

## Installation
1. Download
1. Install into your server's `Mods/UserCode` folder. Make sure file name ends with ".cs". Make sure it does NOT end with ".override.cs"

## Configuration
All configuration should be done by editing the mod source code. Some ideas:

* Modify damage dealt
* Modify energy consumed
* Modify experience gained
* Modify highlight color
* Modify behavior based on animal species
* Adjust Axe melee distance
* Change or remove chat message sent upon dealing damage
* Require players have the Hunter skill to do damage with Axes

Open an issue on GitLab if you need help.

## Future Plans (TODO)
* Hit splats (like the ones shown when an arrow hits an animal)
* Restrict action highlight only to animals
* Weaponize against Nazis

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this mod until certain conditions are met. Contact for additional licensing.

## Credits
This was heavily inspired by the various "Axe Murderer" mods on ██████, however none of their source code was used in the creation of this mod.
