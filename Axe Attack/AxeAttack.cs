using Eco.Gameplay.Animals;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.GameActions;
using Eco.Gameplay.Interactions.Interactors;
using Eco.Gameplay.Players;
using Eco.Shared.Math;
using Eco.Shared.SharedTypes;
using Eco.Simulation.Agents;

namespace Eco.Mods.TechTree {
	public abstract partial class AxeItem {
		[Interaction(
			trigger: InteractionTrigger.LeftClick,
			overrideDescription: "Attack",
			highlightColorHex: InteractionHexColors.Yellow,
			AnimationDriven = true,
			DisallowedEnvVars = new[] { "felled" }
		)]
		public bool @cedd9547_fa09_423a_9690_229c98d6fbfb(Player player, InteractionTriggerInfo triggerInfo, InteractionTarget target) {
			if (target.NetObj is not AnimalEntity animal)
				return false;
			var user = player!.User;
			var action = animal.TryApplyDamage(
				pack: new(),
				damager: player,
				damage: animal.Species.Name == "Fox" ? 0.5f : this.Damage.GetCurrentValue(user),
				target: new() { TargetObjNetID = animal.ID },
				tool: this,
				damageReceived: out float damage_dealt,
				damageDealer: this.GetType()
			);
			var context = this.CreateMultiblockContext(
				player: player,
				applyXPSkill: true,
				position: (Vector3i)animal.Position
			);
			context.ExperienceSkill = typeof(HuntingSkill);
			action.UseTool(context: context);
			var result = action.TryPerform(userToNotify: user);
			var success = result.Success;
			if (success) {
				Animal.AlertNearbyAnimals(animal.Position, 15f);
				user.MsgLoc($"You strike the {animal.Species.Name} for {damage_dealt} damage. {(animal.Health > 0 ? $"{animal.Health} hp remains" : $"The {animal.Species.Name} dies")}.");
			}
			return success;
		}
	}
}
