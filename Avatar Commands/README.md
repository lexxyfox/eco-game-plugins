This mod introduces several new subcommands under `/avatar` command to players that allow them to edit their avatar's UMA definition in ways not allowed by the default Eco character creator, all while inside of an existing save (without having to delete their entire profile and re-joining).

* Change avatar's "gender" field ("timber!" voice line, maybe some other stuff?)
* Change avatar's "race" field (Male / Female body shape)
* Yes, that means that a player's avatar voice need not "match" their body shape
* Change any color on the avatar (any sRGB color!)
* Change face, hair, eyebrows
* Remove eyebrows
* Have (semi-)transparent hair
* Change (some) avatar body proportions

Please leave any comments you might have! I'd love to make this mod more useful if possible, and I'm also interested in what avatar designs are possible.

## Installation
1. Download
1. Install both files into your server's `Mods/UserCode` folder. Make sure file names end with ".cs". Make sure they do NOT end with ".override.cs"

## Configuration
All configuration should be done by editing the mod source code. Some ideas:

* Remove commands you don't want users to have on your server
* Rename commands
* Restrict allowed colors

Open an issue on GitLab if you need help.

## Future Plans (TODO)
* Add safe import and export commands

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this mod until certain conditions are met. Contact for additional licensing.
