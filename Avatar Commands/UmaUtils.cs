using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// this is the mutable collections-based implementation.
namespace d549a56b_f4a0_44e4_b630_c47f9941e655 {

	public class ColorChannel {
		public uint mCol;
		public uint aCol;
	}

	// it's nessecary to keep track of the exact number of channels in a shared color.
	// uma initializes each channel to multiply mask = white, additive mask = black.
	// the first (0th) channel may be white/black, followed by explicitly colored channels.

	public class AvatarDefinition {
		public string race;
		public HashSet<string> wardrobe;
		public Dictionary<string, List<ColorChannel?>> colors;
		public Dictionary<string, int> phenotype;

		public AvatarDefinition() {
			wardrobe = new(StringComparer.CurrentCultureIgnoreCase);
			colors = new(StringComparer.CurrentCultureIgnoreCase);
			phenotype = new(StringComparer.CurrentCultureIgnoreCase);
		}

		public string to_aa_string(string sep = "\n", /* end pos args */ string header = "AA*", string type_sep = ":") {
			var output = new StringBuilder();
			output.Append(header);
			if (wardrobe != null && wardrobe.Count > 0) {
				output.Append(sep);
				output.Append('W');
				output.Append(type_sep);
				output.Append(string.Join(",", wardrobe));
			}
			if (phenotype != null && phenotype.Count > 0) {
				output.Append(sep);
				output.Append('D');
				output.Append(type_sep);
				output.Append(string.Join(";", phenotype.OrderBy(x => x.Key).Select(item => $"{item.Key}={item.Value.ToString("X")}")));
			}
			if (colors != null) {
				// i suppose we're assuming there are no colors with no defined color channels? (todo?)
				foreach (var color in colors.OrderBy(x => x.Key)) {
					output.Append(sep);
					output.Append('C');
					output.Append(type_sep);
					output.Append(color.Key);
					output.Append(',');
					output.Append(color.Value.Count);
					output.Append('=');
					var s = false;
					for (var i = 0; i < color.Value.Count; i++) {
						var c = color.Value[i];
						if (c == null) continue;
						if (s) output.Append(';');
						else s = true;
						output.Append(i);
						output.Append(',');
						output.Append(c.mCol.ToString("X"));
						if (c.aCol == 0) continue;
						output.Append(',');
						output.Append(c.aCol.ToString("X"));
					}
				}
			}
			output.Append(sep);
			output.Append('R');
			output.Append(type_sep);
			output.Append(race);
			return output.ToString();
		}

		public void read_aa_string(string input, char sep = '\n') {
			foreach (var line in input.Split(sep, StringSplitOptions.RemoveEmptyEntries)) {
				var data = line[2..].Trim();
				switch (line[0]) {
					case 'R':
						race = data;
						break;
					case 'W':
						foreach (var w in data.Split(',', StringSplitOptions.RemoveEmptyEntries))
							wardrobe.Add(w);
						break;
					case 'D':
						foreach (var phene in data.Split(';', StringSplitOptions.RemoveEmptyEntries)) {
							var idx = phene.IndexOf('=');
							phenotype[phene[..idx]] = Convert.ToInt32(phene[(idx + 1)..], 16);
						}
						break;
					case 'C':
						var idx1 = data.IndexOf(',');
						var idx2 = data.IndexOf('=');
						var channels = new List<ColorChannel?>(Enumerable.Repeat<ColorChannel?>(null, Convert.ToInt32(data.Substring(idx1 + 1, idx2 - idx1 - 1))));
						colors[data[..idx1]] = channels;
						foreach (var chan_data in data[(idx2 + 1)..].Split(';', StringSplitOptions.RemoveEmptyEntries)) {
							var vals = chan_data.Split(',', StringSplitOptions.RemoveEmptyEntries);
							channels[Convert.ToInt32(vals[0])] = new ColorChannel {
								mCol = Convert.ToUInt32(vals[1], 16),
								aCol = vals.Length < 3 ? 0 : Convert.ToUInt32(vals[2], 16),
							};
						}
						break;
				}
			}
		}

		public static AvatarDefinition from_aa_string(string input, char sep = '\n') {
			var avatar = new AvatarDefinition();
			avatar.read_aa_string(input, sep);
			return avatar;
		}
	}

}
