using d549a56b_f4a0_44e4_b630_c47f9941e655;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Messaging.Chat.Commands;
using Eco.Shared.Items;
using System;
using System.Linq;
using System.Reflection;
using System.Text;

[ChatCommandHandler]
public static class ae774899_3de9_49cf_a8c0_b59cac51901a {

	public const BindingFlags bf = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
	public static PropertyInfo Gender = typeof(Avatar).GetProperty("Gender", bf);
	public static PropertyInfo UmaDefinition = typeof(Avatar).GetProperty("UmaDefinition", bf);
	public static Random random = new();

	[ChatSubCommand("avatar", "Displays your avatar's current voice profile.")]
	public static void gender_get(User user) {
		user.MsgLoc($"Your avatar is {Enum.GetName<AvatarGender>((AvatarGender)Gender.GetValue(user.Avatar))}.");
	}

	[ChatSubCommand("avatar", "Sets your avatar's voice profile. ('Male' or 'Female')")]
	public static void gender_set(User user, string gender) {
		try {
			var g = Enum.Parse<AvatarGender>(gender, true);
			Gender.SetValue(user.Avatar, g);
			user.MsgLoc($"Your avatar is now {Enum.GetName<AvatarGender>((AvatarGender)g)}.");
		}
		catch (Exception) {
			user.ErrorLoc($"Eco does not currently support the '{gender}' gender.");
		}
	}

	public static AvatarDefinition GetAvatarDefinition(Avatar avatar) => AvatarDefinition.from_aa_string(avatar.UmaDefinition);
	public static AvatarDefinition GetAvatarDefinition(User user) => GetAvatarDefinition(user.Avatar);
	public static void SetAvatarDefinition(Avatar avatar, string umadef) => UmaDefinition.SetValue(avatar, umadef);
	public static void SetAvatarDefinition(Avatar avatar, AvatarDefinition umadef) => SetAvatarDefinition(avatar, umadef.to_aa_string(type_sep: ","));
	public static void SetAvatarDefinition(User user, AvatarDefinition umadef) => SetAvatarDefinition(user.Avatar, umadef);

	[ChatSubCommand("avatar", "Displays your avatar's raw UMA definition.")]
	public static void uma_get(User user) {
		user.MsgLocStr(GetAvatarDefinition(user).to_aa_string(header: "", type_sep: ","));
	}

	[ChatSubCommand("avatar", "Sets your avatar's body shape. ('Male' or 'Female')")]
	public static void uma_race_set(User user, string gender) {
		// we're restricting the body shapes available to the user here because
		// inputting a 'race' that Eco doesn't support will make all clients black screen.
		// hopefully SLG fixes this is the future.
		// if you have a mod that adds a body shape to Eco, add its race name below.
		var ad = GetAvatarDefinition(user);
		switch (gender.ToLowerInvariant().Replace("_", "").Replace(" ", "")) {
			case "m":
			case "♂":
			case "man":
			case "male":
			case "ecomale":
				ad.race = "EcoMale";
				break;
			case "f":
			case "♀":
			case "woman":
			case "female":
			case "ecofemale":
				ad.race = "EcoFemale";
				break;
			default:
				// this is here just for funsies
				if (ad.race.StartsWith("r", StringComparison.CurrentCultureIgnoreCase)) {
					ad.race = random.Next(2) == 0 ? "EcoMale" : "EcoFemale";
					break;
				}
				if (ad.race.StartsWith("t", StringComparison.CurrentCultureIgnoreCase)) {
					ad.race = ad.race == "EcoFemale" ? "EcoMale" : "EcoFemale";
					break;
				}
				user.ErrorLocStr("Eco currently only supports the \"Male\" and \"Female\" body shapes. Please select from one of these options, or submit a Feature Suggestion on SLG's feedback tracker.");
				return;
		}
		user.MsgLoc($"Your avatar's body shape has been set to \"{ad.race}\"");
		SetAvatarDefinition(user, ad);
	}

	[ChatSubCommand("avatar", "Displays your avatar's current body shape.")]
	public static void uma_race_get(User user) {
		var ad = GetAvatarDefinition(user);
		user.MsgLocStr(ad.race);
	}

	[ChatSubCommand("avatar", "Displays your avatar's current phenotype.")]
	public static void uma_dna_list(User user) {
		var output = new StringBuilder();
		foreach (var p in GetAvatarDefinition(user).phenotype.OrderBy(x => x.Key)) {
			output.Append('\n');
			output.Append(p.Key);
			output.Append(" = ");
			output.Append(p.Value);
		}
		user.MsgLocStr(output.ToString());
	}

	[ChatSubCommand("avatar", "")]
	public static void uma_dna_set(User user, string name, string val) {
		var avatar = GetAvatarDefinition(user);
		avatar.phenotype[name] = Convert.ToInt32(val);
		SetAvatarDefinition(user, avatar);
	}

	[ChatSubCommand("avatar", "Displays your avatar's current physical features.")]
	public static void uma_wardrobe_list(User user) {
		var output = new StringBuilder();
		foreach (var w in GetAvatarDefinition(user).wardrobe.ToList().OrderBy(_ => _)) {
			output.Append('\n');
			output.Append(w);
		}
		user.MsgLocStr(output.ToString());
	}

	[ChatSubCommand("avatar", "Removes a physical feature from your avatar.")]
	public static void uma_wardrobe_remove(User user, string item) {
		var avatar = GetAvatarDefinition(user);
		avatar.wardrobe.Remove(item);
		SetAvatarDefinition(user, avatar);
	}

	[ChatSubCommand("avatar", "Adds a physical feature to your avatar.")]
	public static void uma_wardrobe_add(User user, string item) {
		var avatar = GetAvatarDefinition(user);
		avatar.wardrobe.Add(item.Replace("\n", ""));
		SetAvatarDefinition(user, avatar);
	}

	public static uint ParseColor(string s) {
		s = s.ToLowerInvariant().Replace("_", "").Replace(" ", "");
		// System.Drawing.Color isn't available on all platforms
		switch (s) {
			#pragma warning disable format
			case "aliceblue"           : return 0xfff0f8ff; case "antiquewhite"        : return 0xfffaebd7;
			case "aqua"                : return 0xff00ffff; case "aquamarine"          : return 0xff7fffd4;
			case "azure"               : return 0xfff0ffff; case "beige"               : return 0xfff5f5dc;
			case "bisque"              : return 0xffffe4c4; case "black"               : return 0xff000000;
			case "blanchedalmond"      : return 0xffffebcd; case "blue"                : return 0xff0000ff;
			case "blueviolet"          : return 0xff8a2be2; case "brown"               : return 0xffa52a2a;
			case "burlywood"           : return 0xffdeb887; case "cadetblue"           : return 0xff5f9ea0;
			case "chartreuse"          : return 0xff7fff00; case "chocolate"           : return 0xffd2691e;
			case "coral"               : return 0xffff7f50; case "cornflowerblue"      : return 0xff6495ed;
			case "cornsilk"            : return 0xfffff8dc; case "crimson"             : return 0xffdc143c;
			case "cyan"                : return 0xff00ffff; case "darkblue"            : return 0xff00008b;
			case "darkcyan"            : return 0xff008b8b; case "darkgoldenrod"       : return 0xffb8860b;
			case "darkgray"            : return 0xffa9a9a9; case "darkgreen"           : return 0xff006400;
			case "darkkhaki"           : return 0xffbdb76b; case "darkmagenta"         : return 0xff8b008b;
			case "darkolivegreen"      : return 0xff556b2f; case "darkorange"          : return 0xffff8c00;
			case "darkorchid"          : return 0xff9932cc; case "darkred"             : return 0xff8b0000;
			case "darksalmon"          : return 0xffe9967a; case "darkseagreen"        : return 0xff8fbc8f;
			case "darkslateblue"       : return 0xff483d8b; case "darkslategray"       : return 0xff2f4f4f;
			case "darkturquoise"       : return 0xff00ced1; case "darkviolet"          : return 0xff9400d3;
			case "deeppink"            : return 0xffff1493; case "deepskyblue"         : return 0xff00bfff;
			case "dimgray"             : return 0xff696969; case "dodgerblue"          : return 0xff1e90ff;
			case "firebrick"           : return 0xffb22222; case "floralwhite"         : return 0xfffffaf0;
			case "forestgreen"         : return 0xff228b22; case "fuchsia"             : return 0xffff00ff;
			case "gainsboro"           : return 0xffdcdcdc; case "ghostwhite"          : return 0xfff8f8ff;
			case "gold"                : return 0xffffd700; case "goldenrod"           : return 0xffdaa520;
			case "gray"                : return 0xff808080; case "green"               : return 0xff008000;
			case "greenyellow"         : return 0xffadff2f; case "honeydew"            : return 0xfff0fff0;
			case "hotpink"             : return 0xffff69b4; case "indianred"           : return 0xffcd5c5c;
			case "indigo"              : return 0xff4b0082; case "ivory"               : return 0xfffffff0;
			case "khaki"               : return 0xfff0e68c; case "lavender"            : return 0xffe6e6fa;
			case "lavenderblush"       : return 0xfffff0f5; case "lawngreen"           : return 0xff7cfc00;
			case "lemonchiffon"        : return 0xfffffacd; case "lightblue"           : return 0xffadd8e6;
			case "lightcoral"          : return 0xfff08080; case "lightcyan"           : return 0xffe0ffff;
			case "lightgoldenrodyellow": return 0xfffafad2; case "lightgray"           : return 0xffd3d3d3;
			case "lightgreen"          : return 0xff90ee90; case "lightpink"           : return 0xffffb6c1;
			case "lightsalmon"         : return 0xffffa07a; case "lightseagreen"       : return 0xff20b2aa;
			case "lightskyblue"        : return 0xff87cefa; case "lightslategray"      : return 0xff778899;
			case "lightsteelblue"      : return 0xffb0c4de; case "lightyellow"         : return 0xffffffe0;
			case "lime"                : return 0xff00ff00; case "limegreen"           : return 0xff32cd32;
			case "linen"               : return 0xfffaf0e6; case "magenta"             : return 0xffff00ff;
			case "maroon"              : return 0xff800000; case "mediumaquamarine"    : return 0xff66cdaa;
			case "mediumblue"          : return 0xff0000cd; case "mediumorchid"        : return 0xffba55d3;
			case "mediumpurple"        : return 0xff9370db; case "mediumseagreen"      : return 0xff3cb371;
			case "mediumslateblue"     : return 0xff7b68ee; case "mediumspringgreen"   : return 0xff00fa9a;
			case "mediumturquoise"     : return 0xff48d1cc; case "mediumvioletred"     : return 0xffc71585;
			case "midnightblue"        : return 0xff191970; case "mintcream"           : return 0xfff5fffa;
			case "mistyrose"           : return 0xffffe4e1; case "moccasin"            : return 0xffffe4b5;
			case "navajowhite"         : return 0xffffdead; case "navy"                : return 0xff000080;
			case "oldlace"             : return 0xfffdf5e6; case "olive"               : return 0xff808000;
			case "olivedrab"           : return 0xff6b8e23; case "orange"              : return 0xffffa500;
			case "orangered"           : return 0xffff4500; case "orchid"              : return 0xffda70d6;
			case "palegoldenrod"       : return 0xffeee8aa; case "palegreen"           : return 0xff98fb98;
			case "paleturquoise"       : return 0xffafeeee; case "palevioletred"       : return 0xffdb7093;
			case "papayawhip"          : return 0xffffefd5; case "peachpuff"           : return 0xffffdab9;
			case "peru"                : return 0xffcd853f; case "pink"                : return 0xffffc0cb;
			case "plum"                : return 0xffdda0dd; case "powderblue"          : return 0xffb0e0e6;
			case "purple"              : return 0xff800080; case "red"                 : return 0xffff0000;
			case "rosybrown"           : return 0xffbc8f8f; case "royalblue"           : return 0xff4169e1;
			case "saddlebrown"         : return 0xff8b4513; case "salmon"              : return 0xfffa8072;
			case "sandybrown"          : return 0xfff4a460; case "seagreen"            : return 0xff2e8b57;
			case "seashell"            : return 0xfffff5ee; case "sienna"              : return 0xffa0522d;
			case "silver"              : return 0xffc0c0c0; case "skyblue"             : return 0xff87ceeb;
			case "slateblue"           : return 0xff6a5acd; case "slategray"           : return 0xff708090;
			case "snow"                : return 0xfffffafa; case "springgreen"         : return 0xff00ff7f;
			case "steelblue"           : return 0xff4682b4; case "tan"                 : return 0xffd2b48c;
			case "teal"                : return 0xff008080; case "thistle"             : return 0xffd8bfd8;
			case "tomato"              : return 0xffff6347; case "turquoise"           : return 0xff40e0d0;
			case "violet"              : return 0xffee82ee; case "wheat"               : return 0xfff5deb3;
			case "white"               : return 0xffffffff; case "whitesmoke"          : return 0xfff5f5f5;
			case "yellow"              : return 0xffffff00; case "yellowgreen"         : return 0xff9acd32;
			#pragma warning restore format
			default:
				if (s.StartsWith("r"))
					return (uint)random.Next(-int.MaxValue, int.MaxValue);
				if (s.StartsWith("0b"))
					return Convert.ToUInt32(s[2..], 2);
				if (s.StartsWith("0o"))
					return Convert.ToUInt32(s[2..], 8);
				if (s.StartsWith("0x"))
					return Convert.ToUInt32(s, 16);
				return Convert.ToUInt32(s);
		}
	}

	[ChatSubCommand("avatar", "Display your avatar's current color values.")]
	public static void uma_mcolor_list(User user) {
		var output = new StringBuilder();
		foreach (var c in GetAvatarDefinition(user).colors.OrderBy(x => x.Key)) {
			output.Append('\n');
			output.Append(c.Key);
			output.Append(" = 0x");
			output.Append(c.Value[0].mCol.ToString("X"));
		}
		user.MsgLocStr(output.ToString());
	}

	[ChatSubCommand("avatar", "Set a color on your avatar. Use either decimal, 0xARGB, or HTML color names.")]
	public static void uma_mcolor_set(User user, string name, string color) {
		var avatar = GetAvatarDefinition(user);
		var name2 = name.Replace(" ", "");
		if (!avatar.colors.ContainsKey(name2)) {
			user.ErrorLoc($"Your avatar does not have a \"{name}\" color slot.");
			return;
		}
		avatar.colors[name2][0].mCol = ParseColor(color);
		SetAvatarDefinition(user, avatar);
	}

	/*
	// This command is 100% safe, but commented out to not overwhelm players
	[ChatSubCommand("avatar", "Randomize all colors on your avatar.")]
	public static void uma_rand_all_mcolors(User user) {
		var avatar = GetAvatarDefinition(user);
		foreach (var color in avatar.colors.Keys)
			avatar.colors[color][0].mCol = (uint) random.Next(-int.MaxValue, int.MaxValue);
		SetAvatarDefinition(user, avatar);
	}
	*/

}
