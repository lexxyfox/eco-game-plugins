## Hewwo!

This is my collection of mods/plugins that I wrote by myself for the video game ██████. This has definitely nothing to do with a game development organization whose name may or may not rhyme with "mange goop inflames". This repository's name simply reflects the fact that all plugins were made with the global ecosystem in mind.

Each folder contains a logically separate plugin. Some of them may be dependant on others. Read each plugin's individual README.md file for a description of what it does, and what steps are necessary for installation.

> "We don’t just sit around and wait for other people. We just make, and we do."
> — <cite>Arlan Hamilton</cite>

> "Stand Up not Punch Down"
> — <cite>Nicky Clark</cite>

This repository is copyright © 2024 Lexxy Fox. All rights reserved.

## Mod descriptions
* Avatar Commands - Allow players to edit their avatar's colors, features, proportions, sex/race, and gender.
* Axe Attack - Allows players to hunt animals using Axes.
* Back to Basics - Starts new players with only an Axe, Pickaxe, and Workbench instead of a fully stocked tent. Requires Redirect Utils.
* Dirt Ramp - Sets the Dirt Ramp recipe to use only 4 dirt instead of 6.
* Disable Rewards - Stop players from collecting their special backer rewards and ██████ entitlements. Requires Redirect Utils.
* Make Heavier - Makes many items heavier and carry-only.
* Redirect Utils - Dependency for several mods, does nothing on its own.

## Typical install instructions
1. Download the files associated with the plugin you want (excluding the README.md file).
1. Copy the files into your server's `Mods/UserCode` folder.

The .cs files can be named anything and be organized in any subdirectory.

You can also download an archive of all of the current files at https://gitlab.com/lexxyfox/eco-game-plugins/-/archive/foxxo/eco-game-plugins-foxxo.tar.gz

## Other great mods (in my opinion)
* OpenNutriView by BeanHeaDza (cannot link)
* uCat's Price Calculator (cannot link)
