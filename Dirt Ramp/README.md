A simple mod that sets the Dirt Ramp recipe to use only 4 dirt instead of 6 at both the Wainwright Table and the Workbench.

## Installation
1. Download
1. Install into your server's `Mods/UserCode` folder. Make sure file name ends with ".cs". Make sure it does NOT end with ".override.cs"

## Configuration
All configuration should be done by editing the mod source code. Some ideas:

* Modify number of dirt used in recipe (default 4)
* Modify number of dirt ramps produced (default 1)

Open an issue on GitLab if you need help.

## Future Plans (TODO)
Currently the mod replaces all the dirt ramp recipes with the 4 dirt recipe, thus conflicting with any other mods that add dirt ramp recipes. In the future I'll change this behavior to instead modify the existing vanilla recipe.

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this mod until certain conditions are met. Contact for additional licensing.
