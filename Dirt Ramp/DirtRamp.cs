namespace Eco.Mods.TechTree {
	public partial class DirtRampRecipe : Eco.Gameplay.Items.Recipes.RecipeFamily { 
		partial void ModsPreInitialize() {
			var recipe = new Eco.Gameplay.Items.Recipes.Recipe();
			recipe.Init(
				name: "DirtRamp",
				displayName: Eco.Shared.Localization.Localizer.DoStr("Dirt Ramp"),
				ingredients: new System.Collections.Generic.List<Eco.Gameplay.Items.Recipes.IngredientElement> {
					new(typeof(DirtItem), 4),
				},
				items: new System.Collections.Generic.List<Eco.Gameplay.Items.Recipes.CraftingElement> {
					new Eco.Gameplay.Items.Recipes.CraftingElement<DirtRampItem>(1),
				}
			);
			this.Recipes = new System.Collections.Generic.List<Eco.Gameplay.Items.Recipes.Recipe> { recipe };
		}
	}
	public partial class DirtRampBasicRecipe : Eco.Gameplay.Items.Recipes.RecipeFamily { 
		partial void ModsPreInitialize() {
			var recipe = new Eco.Gameplay.Items.Recipes.Recipe();
			recipe.Init(
				name: "DirtRamp",
				displayName: Eco.Shared.Localization.Localizer.DoStr("Dirt Ramp"),
				ingredients: new System.Collections.Generic.List<Eco.Gameplay.Items.Recipes.IngredientElement> {
					new(typeof(DirtItem), 4),
				},
				items: new System.Collections.Generic.List<Eco.Gameplay.Items.Recipes.CraftingElement> {
					new Eco.Gameplay.Items.Recipes.CraftingElement<DirtRampItem>(1),
				}
			);
			this.Recipes = new System.Collections.Generic.List<Eco.Gameplay.Items.Recipes.Recipe> { recipe };
		}
	}
}
