Want a more difficult start? Or simply want to start your adventure with a lighter backpack? This mod removes the Starter Camp from new players' starting inventory, and instead provides them only an Axe, Pickaxe, and a Workbench. Players must then proceed to gather resources and craft all other items as necessary. This mod only affects the default new player inventory, so laws and other mods that modify a new player's inventory upon joining the server for the first time will still function correctly. Also consider constructing and utilizing Distribution Station(s).

The Axe and Pickaxe are placed in the player's hotbar for convenience. The Workbench is placed in the backpack.

## Installation
1. Download and install [Redirect Utils](../Redirect Utils)
1. Download
1. Install into your server's `Mods/UserCode` folder. Make sure file name ends with ".cs". Make sure it does NOT end with ".override.cs"

## Configuration
All configuration should be done by editing the mod source. Some ideas:

* Set starting backpack items
* Set starting hot bar items
* Set starting skills

Open an issue on GitLab if you need help.

## Future Plans (TODO)
* Remove a player's "place the starter camp" tutorial, but specifically only that one and leave the other tutorials: movement, zoom, backpack, etc.

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this mod until certain conditions are met. Contact for additional licensing.
