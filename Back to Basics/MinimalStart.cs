using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Mods.TechTree;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using static b383f927_02be_407f_810c_7307b7d8681d;

partial class c1f59315_f34e_465f_b842_c1bb7d8b9078 : IModKitPlugin {

	public string GetStatus() => string.Empty;
	public string GetCategory() => string.Empty;

	public static Dictionary<Type, int> toolbar = new() {
		{ typeof(StoneAxeItem), 1 },
		{ typeof(StonePickaxeItem), 1 },
	};

	public static Dictionary<Type, int> inventory = new() {
		{ typeof(WorkbenchItem), 1 },
	};

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static Dictionary<Type, int> GetDefaultToolbar() => toolbar;

	[MethodImpl(MethodImplOptions.NoInlining)]
	public static Dictionary<Type, int> GetDefaultInventory() => inventory;

	static c1f59315_f34e_465f_b842_c1bb7d8b9078() {
		ReplaceMethod("PlayerDefaults", "GetDefaultToolbar", "c1f59315_f34e_465f_b842_c1bb7d8b9078");
		ReplaceMethod("PlayerDefaults", "GetDefaultInventory", "c1f59315_f34e_465f_b842_c1bb7d8b9078");
	}

}

