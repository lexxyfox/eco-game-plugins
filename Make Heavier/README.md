This mod increases the mass of many items (sometimes significantly!), as well as makes many items carry-only.

| Item | Carry? | Old Mass | New Mass |
| ---- | ------ | -------- | -------- |
| Arrastra                | Y | 5  |   63¾ |
| Bison Carcass           | Y | 5½ | 1000  | 
| Butchery Table          | Y | 2  |   25  |
| Campfire                | Y | 2  |   28  |
| Carpentry Table         | Y | 2  |   80  |
| Farmer's Table          | Y | 2  |  110  |
| Fiber Scutching Station | Y | 5  |   25  |
| Fletching Table         | Y | 2  |   25  |
| Grindstone              | Y | 2  |   72½ |
| Hewn Log Door           | N | 1  |   10  |
| Icebox                  | Y | 1  |   20  |
| Loom                    | Y | 5  |   12¾ |
| Masonry Table           | Y | 2  |   65  |
| Research Table          | Y | 2  |  135¼ |
| Storage Chest           | Y | 1  |   25  |
| Store                   | Y | 2  |   40  |
| Tailoring Table         | Y | 2  |   12¾ |
| Wainwright Table        | Y | 2  |   25  |
| Work Bench              | N | 1  |   25  |

## Installation
1. Download
1. Install into your server's `Mods/UserCode` folder. Make sure the file names ends with ".cs". Make sure they do NOT end with ".override.cs"

## Configuration
All configuration should be done by editing the mod source code. 

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this software for any reason. Contact for additional licensing.
