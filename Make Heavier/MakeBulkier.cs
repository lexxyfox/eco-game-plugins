
using Eco.Gameplay.Items;

namespace Eco.Mods.TechTree {

    [Carried] partial class ArrastraItem { }
    [Carried] partial class BisonCarcassItem { }
    [Carried] partial class ButcheryTableItem { }
    [Carried] partial class CampfireItem { }
    [Carried] partial class CarpentryTableItem { }
    [Carried] partial class FarmersTableItem { }
    [Carried] partial class FiberScutchingStationItem { }
    [Carried] partial class FletchingTableItem { }
    [Carried] partial class GrindstoneItem { }
    [Carried] partial class IceboxItem { }
    [Carried] partial class LoomItem { }
    [Carried] partial class MasonryTableItem { }
    [Carried] partial class ResearchTableItem { }
    [Carried] partial class StorageChestItem { }
    [Carried] partial class StoreItem { }
    [Carried] partial class TailoringTableItem { }
    [Carried] partial class ToolBenchItem { }
    [Carried] partial class WainwrightTableItem { }

}
