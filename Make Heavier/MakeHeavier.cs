using System.Reflection;
using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils.Logging;
using Eco.Gameplay.Items;
using Eco.Mods.TechTree;

class af20240f_400e_4346_b131_5abfb77cc248 : IModKitPlugin {
    public string GetStatus() => string.Empty;
    public string GetCategory() => string.Empty;

    public static readonly PropertyInfo mass = typeof(WeightAttribute).GetProperty("Weight");
    public static readonly BindingFlags bf = BindingFlags.NonPublic | BindingFlags.Instance;

    public static void set_mass<T>(int m) {
        ConsoleLogWriter.Instance.Write($"Set mass of {typeof(T).Name} to {m / 1000}kg\n");
        mass.SetValue(
            ItemAttribute.Get<WeightAttribute>(typeof(T)),
            m, bf, null, null, null
        );
    }
    private static void set_mass<T>(double m) => set_mass<T>((int) m);

    static af20240f_400e_4346_b131_5abfb77cc248() {
        var mass_plantfiber = 30;
        var mass_stone = 6000;
        var mass_wood = 10000;
        var mass_yarn = 30;
        var mass_dirt = 19000;
        var mult_workbench = 0.5;
        var mult_woodworking = 0.5;
        var mult_masonry = 0.5;

        var mass_hewnlog = mass_wood * mult_woodworking;
        var mass_board = mass_wood * mult_woodworking / 2;

        set_mass<ArrastraItem>((mass_wood * 2.5 + mass_stone * 15 + mass_board * 5) * mult_workbench);
        set_mass<ButcheryTableItem>((mass_hewnlog * 5 + mass_board * 10) * mult_woodworking);
        set_mass<CampfireItem>((mass_wood * 2 + mass_stone * 6) * mult_workbench);
        set_mass<CarpentryTableItem>((mass_wood * 10 + mass_stone * 10) * mult_workbench);
        set_mass<FarmersTableItem>((mass_hewnlog * 4 + mass_board * 4 + mass_dirt * 10) * mult_workbench);
        set_mass<FiberScutchingStationItem>((mass_hewnlog * 5 + mass_board * 10) * mult_woodworking);
        set_mass<FletchingTableItem>((mass_hewnlog * 5 + mass_board * 10) * mult_woodworking);
        set_mass<GrindstoneItem>((mass_hewnlog * 2.5 + mass_board * 5 + mass_stone * 20) * mult_masonry);
        set_mass<HewnDoorItem>(mass_hewnlog * 2); // no multiplier
        set_mass<HewnHardwoodDoorItem>(mass_hewnlog * 2); // no multiplier
        set_mass<HewnSoftwoodDoorItem>(mass_hewnlog * 2); // no multiplier
        set_mass<IceboxItem>((mass_hewnlog * 5 + mass_board * 6) * mult_woodworking);
        set_mass<LoomItem>((mass_board * 10 + mass_yarn * 10) * mult_woodworking);
        set_mass<MasonryTableItem>((mass_wood * 10 + mass_stone * 5) * mult_workbench);
        set_mass<ResearchTableItem>((mass_plantfiber * 15 + mass_wood * 15 + mass_stone * 20) * mult_workbench);
        set_mass<StorageChestItem>(mass_wood * 5 * mult_workbench);
        set_mass<StoreItem>((mass_wood * 5 + mass_stone * 5) * mult_workbench);
        set_mass<TailoringTableItem>((mass_board * 10 + mass_plantfiber * 25) * mult_woodworking);
        set_mass<ToolBenchItem>((mass_wood * 10 + mass_stone * 5) * mult_workbench);
        set_mass<WainwrightTableItem>((mass_hewnlog * 7.5 + mass_board * 5) * mult_woodworking);
        set_mass<WorkbenchItem>(25000);

        // I guess I'm using live weight???
        // Dressed/Carcass weight is weight after the removal of hide, head, feet, and intestines.
        // But Eco implies that is done at the Buthercy Table, and the head and hide and feet are still present because it can be used to make Mounted and Stuffed animals.
        set_mass<BisonCarcassItem>(1000000);
    }

}
