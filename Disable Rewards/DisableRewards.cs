using Eco.Core.Plugins.Interfaces;
using System.Runtime.CompilerServices;
using static b383f927_02be_407f_810c_7307b7d8681d;

class b12afeef_a119_48ef_b77a_db9171828557 : IModKitPlugin {

	public string GetStatus() => string.Empty;
	public string GetCategory() => string.Empty;

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private void AttemptToGrantAccountInventoryItems() {}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public int @as() => 0;

	static b12afeef_a119_48ef_b77a_db9171828557() {
		var type = typeof(b12afeef_a119_48ef_b77a_db9171828557);
		var user = typeof(Eco.Gameplay.Players.User);
		ReplaceMethod(user, "AttemptToGrantAccountInventoryItems", type);
		ReplaceMethod(user.GetProperty("AccountLevel").GetMethod, type, "as");
	}

}

