This mod prevents players from collecting their special Alpha Backer rewards, Wolf Whisperer rewards, and ████████ entitlements, as well as prevent ████████ from ████████. This mod does not remove these items from existing inventories such as those collected by players from an existing world before this mod was installed.

If you enjoy playing ████████ be sure to ████████ the developers with a Wolf ████████, and/or the ████████ of additional ████████.

## Installation
1. Download and install [Redirect Utils](../Redirect Utils)
1. Download
1. Install into your server's `Mods/UserCode` folder. Make sure file name ends with ".cs". Make sure it does NOT end with ".override.cs"

## Configuration
All configuration should be done by editing the mod source code. Some ideas:

* ████████
* ████████
* ████████
* ████████

## Future Plans (TODO)
* ████████

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this software for any reason. Contact for additional licensing.
