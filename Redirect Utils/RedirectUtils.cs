using System;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Runtime.CompilerServices;

public static class b383f927_02be_407f_810c_7307b7d8681d {

	public const BindingFlags ANY =
		BindingFlags.Instance |
		BindingFlags.Static |
		BindingFlags.Public |
		BindingFlags.NonPublic;

	public static void ReplaceMethod(RuntimeMethodHandle source, RuntimeMethodHandle target) {
		RuntimeHelpers.PrepareMethod(source);
		RuntimeHelpers.PrepareMethod(target);
		Marshal.WriteIntPtr(source.Value, IntPtr.Size * 2, target.GetFunctionPointer());
	}

	public static void ReplaceMethod(MethodInfo source, MethodInfo target) =>
		ReplaceMethod(source.MethodHandle, target.MethodHandle);

	public static void ReplaceMethod(MethodInfo source, Type target_type, string target_method) =>
		ReplaceMethod(source, target_type.GetMethod(target_method, ANY));

	public static void ReplaceMethod(Type source_type, string source_method, Type target_type, string target_method) =>
		ReplaceMethod(source_type.GetMethod(source_method, ANY), target_type.GetMethod(target_method, ANY));

	public static void ReplaceMethod(Type source, string method_name, Type target) =>
		ReplaceMethod(source, method_name, target, method_name);

	public static Type GetType(string name) {
		foreach (var ass in AppDomain.CurrentDomain.GetAssemblies()) {
			var t = ass.GetType(name);
			if (t is not null) return t;
		}
		foreach (var ass in AppDomain.CurrentDomain.GetAssemblies()) {
			var t = ass.GetType(name, false, true);
			if (t is not null) return t;
		}
		return null;
	}

	public static void ReplaceMethod(string source_type, string source_method, Type target_type, string target_method) =>
		ReplaceMethod(GetType(source_type), source_method, target_type, target_method);

	public static void ReplaceMethod(string source, string method_name, Type target) =>
		ReplaceMethod(GetType(source), method_name, target);

	public static void ReplaceMethod(string source, string method_name, string target) =>
		ReplaceMethod(GetType(source), method_name, GetType(target));

}
