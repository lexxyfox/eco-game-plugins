This is a utility class that's required by some of my mods.

## Installation
1. Download
1. Install into your server's `Mods/UserCode` folder. Make sure file name ends with ".cs". Make sure it does NOT end with ".override.cs"

## License
All code is original production by me. All rights reserved. SLG employees and associates are not allowed to download or use this software for any reason. Contact for additional licensing.
